//
//  ViewController.swift
//  iOSBootcamp3-architecture
//
//  Created by Marcel Starczyk on 13.02.2018.
//  Copyright © 2018 Droids On Roids. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!

    let people = ["Marcin Chojnacki", "Łukasz Mróz", "Maka Wędkarz"]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basicCellIdentifier", for: indexPath)
        cell.textLabel?.text = people[indexPath.row]

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let person = people[indexPath.row]

        performSegue(withIdentifier: "personDetailSegue", sender: person)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let person = sender as? String,
            let controller = segue.destination as? PersonDetailViewController else { return }

        controller.person = person
    }
}


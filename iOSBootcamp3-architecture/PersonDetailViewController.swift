//
//  PersonDetailViewController.swift
//  iOSBootcamp3-architecture
//
//  Created by Marcel Starczyk on 13.02.2018.
//  Copyright © 2018 Droids On Roids. All rights reserved.
//

import UIKit

class PersonDetailViewController: UIViewController {

    var person: String?

    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        personNameLabel.text = person
        switch person {
        case "Marcin Chojnacki"?:
            avatarImageView.image = #imageLiteral(resourceName: "thinking_frost")
        case "Łukasz Mróz"?:
            avatarImageView.image = #imageLiteral(resourceName: "maka_wedkarz")
        case "Maka Wędkarz"?:
            avatarImageView.image = #imageLiteral(resourceName: "bane")
        default:
            avatarImageView.backgroundColor = .blue
        }
    }
}
